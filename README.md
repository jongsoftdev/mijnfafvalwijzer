# Mijn Afvalwijzer Component
This custom component enables parsing and displaying of trash collection dates from the [MijnAfvalWijzer ](http://mijnafvalwijzer.nl) site. The component will regularly scan the JSON feed to update the next trash pickup date.

## Installation instructions

* Download one of the releases of this component
* Upload the component in the following directory on home assistant

```
/config
```

## Configuration example

The following variables can be monitored:

* restafval, used for 'Restafval'
* gft, used for 'Groente, Fruit & Tuin'
* restgft, for 'Duobak'
* plastic, used for 'Plastic'
* papier, used for 'Papier'
* pmd

It is also possible to register sensors to display which trash gets collected on a specific day from now.
This could be used to add trash collected today, tomorrow, etc. To do this add the `ingepland` configuration, containing the amount of days in the future
to monitor. This can be a comma seperated list of numbers.

The component will automatically register sensors for the pickup dates as well as the following:

* trash_notify, a switch that allows for easier creation of automation based upon pickup dates

```yaml
mijnafvalwijzer:
  postcode: !secret address_po
  huisnummer: !secret address_hn
  monitored_variables:
    - restgft
    - plastic
    - papier
  ingepland: 0,1
```

### Example group configuration
The sample below shows how you could group the information of the custom component into one single card:

```yaml
name: Garbage collection
control: hidden
entities:
  - switch.trash_notify
  - sensor.trash_restgft
  - sensor.trash_papier
  - sensor.trash_plastic
```

### Example automation
The sample automation below will notify the user at 20:00 the day before the paper is collected.

```yaml
alias: 'Trash - Papier'
hide_entity: true
trigger:
  - platform: time
    hours: 20
    minutes: 0
    seconds: 0
condition:
  - condition: state
    entity_id: switch.trash_notify
    state: 'on'
  - condition: template
    value_template: '{{ now().strftime("%Y-%m-%d") == (as_timestamp(states.sensor.trash_papier.attributes.next_pickup) - (24*3600)) | timestamp_custom("%Y-%m-%d") }}'
action:
  - service: notify.firebase
    data_template:
      message: 'Het papier wordt morgen opgehaald.'
    data:
      title: 'Trash notification'
      data:
        tag: 'trash-collection-tomorrow'
```
