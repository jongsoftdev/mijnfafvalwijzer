from homeassistant.components.switch import SwitchDevice

SENSOR_PREFIX = 'trash_'


def setup_platform(hass, config, add_devices, discovery_info=None):
    sensors = [MyTrashToggle('Trash notification', SENSOR_PREFIX + 'notify', True, 'mdi:message')]
    add_devices(sensors, True)


class MyTrashToggle(SwitchDevice):

    def __init__(self, name, entity_id, initial, icon):
        self._name = name
        self._state = initial
        self._icon = icon
        self.entity_id = 'switch.' + entity_id

    @property
    def is_on(self) -> bool:
        return self._state

    @property
    def icon(self):
        return self._icon

    @property
    def name(self):
        return self._name

    def turn_on(self, **kwargs) -> None:
        self._state = True

    def turn_off(self, **kwargs) -> None:
        self._state = False
