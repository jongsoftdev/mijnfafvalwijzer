"""
@ Author      : Gerben Jongerius
@ Date        : 01/18/2018
@ Description : MijnAfvalwijzer Sensor - It queries mijnafvalwijzer.nl.
"""
import datetime
import logging

from homeassistant.helpers.entity import Entity
from homeassistant.util.dt import as_timestamp, parse_datetime

import custom_components.mijnafvalwijzer as mijnafvalwijzer

SENSOR_PREFIX = 'trash_'
_LOGGER = logging.getLogger(__name__)


def setup_platform(hass, config, add_devices, discovery_info=None):
    data_collector = hass.data[mijnafvalwijzer.DATA_MFH]

    sensors = []
    # Add the trash sensors to the system
    for trash_sensor in data_collector.get_enabled_sensors():
        sensor = data_collector.get_sensors()[trash_sensor]
        sensors.append(MyTrashSensor(trash_sensor, sensor, data_collector, hass))

    # Add the day interval sensors
    for day_interval in data_collector.get_enabled_days():
        sensors.append(DateTrashSensor(data_collector, day_interval))

    add_devices(sensors, True)


class DateTrashSensor(Entity):

    def __init__(self, collector, date_offset_days):
        self._state = []
        self._config = collector
        self._date_offset = date_offset_days
        self._matching_sensors = []
        self.entity_id = 'sensor.' + SENSOR_PREFIX + 'day_' + str(date_offset_days)

    @property
    def name(self):
        if self._date_offset == 0:
            return 'Today'
        if self._date_offset == 1:
            return 'Tomorrow'

        return 'In ' + self._date_offset + ' days'

    @property
    def state(self):
        return ', '.join(str(x) for x in self._state)

    @property
    def device_state_attributes(self):
        return {
            'trash_types': self._matching_sensors,
            'date_offset': self._date_offset
        }

    def update(self):
        self._matching_sensors = []
        self._state = []
        expected_date = datetime.datetime.today()
        if self._date_offset > 0:
            expected_date = expected_date + datetime.timedelta(days=self._date_offset)

        for pickup in self._config.data.get('trash_dates'):
            pickup_date = parse_datetime(str(pickup['date']))
            if pickup_date.date() == expected_date.date():
                self._matching_sensors.append(pickup['type'])
                self._state.append(self._config.get_sensors()[pickup['type']][0])


class MyTrashSensor(Entity):

    def __init__(self, name, trash_type, collector, hass):
        self._state = None
        self._hass = hass
        self._name = name
        self._type = trash_type
        self._collector = collector
        self._nextDate = None
        self._eventFired = False
        self.entity_id = 'sensor.' + SENSOR_PREFIX + name

    @property
    def name(self):
        return self._type[0]

    @property
    def icon(self):
        return self._type[1]

    @property
    def state(self):
        return self._state

    @property
    def entity_picture(self):
        return '/local/mijnafvalwijzer/' + str(self._name) + '_80.png'

    @property
    def device_state_attributes(self):
        return {
            'next_pickup': self._nextDate,
            'event_fired': self._eventFired
        }

    def update(self):
        _LOGGER.debug("Updating trash sensor " + self._name)
        self._collector.update()
        previous_val = self._nextDate or datetime.datetime.today()
        for pickup in self._collector.data.get('trash_dates'):
            if pickup['type'] == self._name:
                self._nextDate = pickup['date']
                self._state = pickup['display']

        if previous_val != self._nextDate:
            tomorrow = datetime.datetime.today() + datetime.timedelta(days=1)
            scheduled = datetime.datetime.fromtimestamp(as_timestamp(self._nextDate))
            if (tomorrow - scheduled).days == 0 :
                _LOGGER.debug("Firing AfvalWijzer.nl event for trash collection for " + self._name)
                self._eventFired = True
                self._hass.bus.fire(mijnafvalwijzer.EVENT_TRASH_COLLECTING, {
                    'type': self._type,
                    'date': self._nextDate,
                    'name': self._name
                })

