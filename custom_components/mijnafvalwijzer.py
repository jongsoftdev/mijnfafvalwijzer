"""
@ Author      : Gerben Jongerius
@ Date        : 01/18/2018
@ Description : MijnAfvalwijzer Sensor - It queries mijnafvalwijzer.nl.
"""
import json
import logging
from datetime import timedelta, datetime
from urllib.request import urlopen

import homeassistant.helpers.config_validation as cv
import voluptuous as vol
from homeassistant.const import CONF_MONITORED_VARIABLES, CONF_NAME
from homeassistant.helpers.discovery import load_platform
from homeassistant.util import Throttle

DOMAIN = 'mijnafvalwijzer'
DATA_MFH = 'trash_config'
SCAN_INTERVAL = timedelta(days=1)
DEFAULT_NAME = 'MijnAvfalwijzer'

EVENT_TRASH_COLLECTING = 'trash_collection'

# Configuration options
CONF_POSTCODE = 'postcode'
CONF_HOUSE_NUMBER = 'huisnummer'
CONF_DAY_SENSOR = 'ingepland'
_LOGGER = logging.getLogger(__name__)

CONFIG_SCHEMA = vol.Schema({
    DOMAIN: vol.Schema({
        vol.Optional(CONF_NAME, default=DEFAULT_NAME): cv.string,
        vol.Optional(CONF_POSTCODE): cv.string,
        vol.Optional(CONF_HOUSE_NUMBER): cv.string,
        vol.Optional(CONF_DAY_SENSOR, default=[0]): cv.string,
        vol.Optional(CONF_MONITORED_VARIABLES, default=['restafval', 'gft']): vol.All(
            cv.ensure_list, vol.Length(min=1), [vol.In(['restafval', 'gft', 'plastic', 'papier', 'pmd', 'restgft'])])
    })
}, extra=vol.ALLOW_EXTRA)


def setup(hass, config):
    _LOGGER.debug("Starting up the MijnAfvalwijzer sensors")
    conf = config.get(DOMAIN, {})

    enabled_days = conf.get(CONF_DAY_SENSOR, "").split(",")
    hass.data[DATA_MFH] = MyTrashConfig(conf.get(CONF_POSTCODE), conf.get(CONF_HOUSE_NUMBER),
                                        conf.get(CONF_MONITORED_VARIABLES), enabled_days)

    load_platform(hass, 'sensor', DOMAIN)
    load_platform(hass, 'switch', DOMAIN)
    return True


class MyTrashConfig(object):

    def __init__(self, postal_code, house_number, enabled_sensors, enabled_days):
        self._sensor_types = {
            'restafval': ['Restafval', 'mdi:delete-empty'],
            'gft': ['Groente Fruit & Tuin', 'mdi:delete-empty'],
            'plastic': ['Plastic', 'mdi:delete-empty'],
            'papier': ['Oud Papier', 'mdi:newspaper'],
            'pmd': ['Plastic, metaal en drank karton', 'mdi:delete-empty'],
            'restgft': ['Duobak', 'mdi:delete-empty']
        }
        self._url = "http://json.mijnafvalwijzer.nl/?method=postcodecheck&postcode={0}&street=&huisnummer={1}&toevoeging=&platform=phone&langs=nl&".format(
            postal_code, house_number)
        self._enabled_sensors = enabled_sensors
        self._enabled_days = []
        self.data = {
            'trash_dates': []
        }

        for raw_day in enabled_days:
            if raw_day.isdigit():
                self._enabled_days.append(int(raw_day.strip()))

    def get_sensors(self):
        return self._sensor_types

    def get_enabled_sensors(self):
        return self._enabled_sensors

    def get_enabled_days(self):
        return self._enabled_days

    @Throttle(SCAN_INTERVAL)
    def update(self):
        raw_res = urlopen(self._url)
        today = datetime.today().strftime("%Y-%m-%d")
        data = json.loads(raw_res.read().decode('utf-8'))

        self.data['trash_dates'] = []
        # Store the garbage collection dates in the internal data store
        for trash_sensor in self._sensor_types:
            for item in data['data']['ophaaldagen']['data'] or data['data']['ophaaldagenNext']['data']:
                if item['date'] > today and item['type'] == trash_sensor:
                    self.data['trash_dates'].append({
                        'type': trash_sensor,
                        'date': (str(datetime.strptime(item['date'], "%Y-%m-%d"))),
                        'display': item['date']
                    })
                    break
